Synopsis
       Simple blockchain in powershell
DESCRIPTION
       Can add blocks to the blockchain with any string data in the data field
       Can also verify the integrity of the blockchain to prevent fake or corrupted blocks
       
       Available functions:
       generate-nextblock -data
       hash -textToHash
       get-genesisBlock
       calculate-hash -index -previousHash -timestamp -data
       get-latestBlock
       generate-nextBlock -blockdata
       check-sameBlock -block1 -block2
       check-validNewBlock -newBlock -previousBlock
       check-validChain -blockchainToValidate

       Available variables
       $blockchain - array with blocks
NOTES
        Author:  Jack Swedjemark         
        Updated: 13/12/2017
        PSVer:   5.0     
